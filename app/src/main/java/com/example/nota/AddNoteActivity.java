package com.example.nota;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AddNoteActivity extends AppCompatActivity {
    private ConstraintLayout add_note;
    private Button buttonSaveNote;
    private Button buttonDiscardNote;
    private Button buttonReminderTime;
    private Switch switchReminder;
    private EditText editTitle;
    private EditText editText;
    private NotesOperations notesOperations;
    private Note note;
    String title;
    String text;
    int minute;
    int hour;
    int day;
    int month;
    int year;
    boolean reminderTimePresent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        notesOperations = MainActivity.notesOperations;

        // Obtain the views of all widgets at startup
        add_note = (ConstraintLayout) findViewById(R.id.add_note);
        switchReminder = (Switch) findViewById(R.id.switchReminder);
        buttonSaveNote = (Button) findViewById(R.id.buttonSaveNote);
        buttonDiscardNote = (Button) findViewById(R.id.buttonDiscardNote);
        editTitle = (EditText) findViewById(R.id.editTitle);
        editTitle.setFilters(new InputFilter[] {new InputFilter.LengthFilter(20)});
        editText = (EditText) findViewById(R.id.editText);

        // Load variables if already exist
        SharedPreferences sp = getSharedPreferences("add_note",Context.MODE_PRIVATE);
        text = sp.getString("text","");
        title = sp.getString("title","");
        reminderTimePresent = sp.getBoolean("reminderTimePresent",false);
        minute = sp.getInt("minute",-1);
        hour = sp.getInt("hour",-1);
        day = sp.getInt("day",-1);
        month = sp.getInt("month",-1);
        year = sp.getInt("year",-1);

        // Display loaded variables for user
        editText.setText(text);
        editTitle.setText(title);
        if (reminderTimePresent) {
            activateReminderButton(switchReminder);
        }

        // Activate listener for switch
        switchReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent toInputTimeActivity = new Intent();
                    toInputTimeActivity.setClass(getApplicationContext(),InputReminderActivity.class);
                    toInputTimeActivity.putExtra("reminderTimePresent",reminderTimePresent);
                    if (reminderTimePresent) {
                        toInputTimeActivity.putExtra("minute",minute);
                        toInputTimeActivity.putExtra("hour",hour);
                        toInputTimeActivity.putExtra("day",day);
                        toInputTimeActivity.putExtra("month",month);
                        toInputTimeActivity.putExtra("year",year);
                    }
                    startActivityForResult(toInputTimeActivity,1);
                } else {
                    if (buttonReminderTime != null) {
                        destroyReminderButton();
                    }
                }
            }
        });

        buttonSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonDiscardNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),R.string.draft_discarded,Toast.LENGTH_SHORT).show();

                SharedPreferences sp = getSharedPreferences("add_note",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.commit();

                Intent i = new Intent();
                i.setClass(getApplicationContext(),MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            boolean fromReminderButton = data.getBooleanExtra("fromReminderButton",false);
            if (resultCode == RESULT_CANCELED) {
                if (fromReminderButton) {} else {
                    switchReminder.setChecked(false);
                }
            } else {
                minute = data.getIntExtra("minute",0);
                hour = data.getIntExtra("hour",0);
                day = data.getIntExtra("day",0);
                month = data.getIntExtra("month",0);
                year = data.getIntExtra("year",0);
                SharedPreferences sp = getSharedPreferences("add_note",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt("minute",minute);
                editor.putInt("hour",hour);
                editor.putInt("day",day);
                editor.putInt("month",month);
                editor.putInt("year",year);
                editor.commit();
                reminderTimePresent = true;
                activateReminderButton(switchReminder);
            }
        }
    }

    @Override
    public void onBackPressed() {
        String title = editTitle.getText().toString();
        String text = editText.getText().toString();
        Note note;
        if (text.equals("") == false) {
            SharedPreferences sp = getSharedPreferences("add_note",Context.MODE_PRIVATE);
            year = sp.getInt("year",0);
            month = sp.getInt("month",0);
            day = sp.getInt("day",0);
            hour = sp.getInt("hour",0);
            minute = sp.getInt("minute",0);
            Switch switchReminder = (Switch) findViewById(R.id.switchReminder);
            if (title.length() == 0) {
                if (switchReminder.isChecked()) {
                    note = new Note(text, year, month, day, hour, minute);
                } else {
                    note = new Note(text);
                }
            } else {
                if (switchReminder.isChecked()) {
                    note = new Note(title, text, year, month, day, hour, minute);
                } else {
                    note = new Note(title, text);
                }
            }
            notesOperations.open();
            notesOperations.modifyNote(note,0);
            notesOperations.close();
            Toast.makeText(getApplicationContext(),R.string.note_saved, Toast.LENGTH_SHORT).show();

            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            editor.commit();

            Intent i = new Intent();
            i.setClass(getApplicationContext(),MainActivity.class);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(getApplicationContext(),R.string.draft_discarded,Toast.LENGTH_SHORT).show();

            SharedPreferences sp = getSharedPreferences("add_note",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            editor.commit();

            Intent i = new Intent();
            i.setClass(getApplicationContext(),MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        SharedPreferences sp = getSharedPreferences("add_note",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
        super.onDestroy();
    }

    public void activateReminderButton(Switch switchReminder) {

        // Set switch to on
        switchReminder.setChecked(true);

        if (buttonReminderTime != null) {
            buttonReminderTime.setVisibility(View.VISIBLE);
            LocalDateTime timeToDisplay = LocalDateTime.of(year,month,day,hour,minute);
            buttonReminderTime.setText(timeToDisplay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        } else {
            // Add new button to indicate reminderTime and allow user to click on it to change it
            buttonReminderTime = new Button(getApplicationContext());
            buttonReminderTime.setId(R.id.buttonReminderTime);
            add_note.addView(buttonReminderTime);
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(add_note);
            constraintSet.connect(buttonReminderTime.getId(),ConstraintSet.RIGHT,R.id.add_note,ConstraintSet.RIGHT,0);
            constraintSet.connect(buttonReminderTime.getId(),ConstraintSet.LEFT,R.id.add_note,ConstraintSet.LEFT,0);
            constraintSet.connect(buttonReminderTime.getId(),ConstraintSet.TOP,R.id.switchReminder,ConstraintSet.BOTTOM,0);
            constraintSet.applyTo(add_note);
            LocalDateTime timeToDisplay = LocalDateTime.of(year,month,day,hour,minute);
            buttonReminderTime.setText(timeToDisplay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        }

        buttonReminderTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toInputTimeActivity = new Intent();
                toInputTimeActivity.setClass(getApplicationContext(),InputReminderActivity.class);
                toInputTimeActivity.putExtra("reminderTimePresent",reminderTimePresent);
                if (reminderTimePresent) {
                    toInputTimeActivity.putExtra("minute",minute);
                    toInputTimeActivity.putExtra("hour",hour);
                    toInputTimeActivity.putExtra("day",day);
                    toInputTimeActivity.putExtra("month",month);
                    toInputTimeActivity.putExtra("year",year);
                    toInputTimeActivity.putExtra("fromReminderButton",true);
                }
                startActivityForResult(toInputTimeActivity,1);
            }
        });
    }

    public void destroyReminderButton() {
        buttonReminderTime.setVisibility(View.GONE);
    }
}