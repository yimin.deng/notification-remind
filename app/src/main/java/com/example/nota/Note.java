package com.example.nota;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class Note implements Serializable {

    private int id;
    private String title;
    private String text;
    private LocalDateTime lastModified;
    private LocalDateTime reminderTime;

    public Note(String text) {
        this.text = text;
        this.lastModified = LocalDateTime.now(ZoneOffset.UTC);
    }

    public Note(String title,String text) {
        this.title = title;
        this.text = text;
        this.lastModified = LocalDateTime.now(ZoneOffset.UTC);
    }

    public Note(String text,int year,int month,int dayOfMonth,int hour,int minute) {
        this.text = text;
        this.lastModified = LocalDateTime.now(ZoneOffset.UTC);
        this.reminderTime = LocalDateTime.of(year,month,dayOfMonth,hour,minute).atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
    }

    public Note(String title,String text,int year,int month,int dayOfMonth,int hour,int minute) {
        this.title = title;
        this.text = text;
        this.lastModified = LocalDateTime.now(ZoneOffset.UTC);
        this.reminderTime = LocalDateTime.of(year,month,dayOfMonth,hour,minute).atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
    }

    public Note(int id,String text,LocalDateTime lastModified) {
        this.id = id;
        this.text = text;
        this.lastModified = lastModified;
    }

    public Note(int id,String title,String text,LocalDateTime lastModified) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.lastModified = lastModified;
    }

    public Note(int id,String text,LocalDateTime lastModified,LocalDateTime reminderTime) {
        this.id = id;
        this.text = text;
        this.lastModified = lastModified;
        this.reminderTime = reminderTime;
    }

    public Note(int id,String title,String text,LocalDateTime lastModified,LocalDateTime reminderTime) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.lastModified = lastModified;
        this.reminderTime = reminderTime;
    }

    @Override
    public String toString() {
        String textToDisplay;
        if (text.length() > 50) {
            textToDisplay = text.substring(0,50) + "...";
        } else {
            textToDisplay = text;
        }
        if (title != null) {
            if ((reminderTime != null) && (reminderTime.isAfter(LocalDateTime.now(ZoneOffset.UTC)))) {
                return "[ " + title + " ]\n" + textToDisplay + "\n" + reminderTime.atZone(ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
            } else {
                return "[ " + title + " ]\n" + textToDisplay;
            }
        } else {
            if ((reminderTime != null) && (reminderTime.isAfter(LocalDateTime.now(ZoneOffset.UTC)))) {
                return textToDisplay + "\n" + reminderTime.atZone(ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
            } else {
                return textToDisplay;
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified() {
        this.lastModified = LocalDateTime.now(ZoneOffset.UTC);
    }

    public LocalDateTime getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(int year,int month,int dayOfMonth,int hour,int minute) {
        this.reminderTime = LocalDateTime.of(year,month,dayOfMonth,hour,minute).atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
    }

}
