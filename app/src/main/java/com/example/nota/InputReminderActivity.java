package com.example.nota;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class InputReminderActivity extends AppCompatActivity {
    private boolean fromReminderButton;
    private boolean reminderTimePresent;
    int minute;
    int hour;
    int day;
    int month;
    int year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_reminder);

        Intent i = getIntent();
        fromReminderButton = i.getBooleanExtra("fromReminderButton",false);
        reminderTimePresent = i.getBooleanExtra("reminderTimePresent",false);
        if (reminderTimePresent) {
            minute = i.getIntExtra("minute",-1);
            hour = i.getIntExtra("hour",-1);
            day = i.getIntExtra("day",-1);
            month = i.getIntExtra("month",-1) - 1;
            year = i.getIntExtra("year",-1);
        }

        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);

        if (reminderTimePresent) {
            timePicker.setMinute(minute);
            timePicker.setHour(hour);
            datePicker.updateDate(year,month,day);
        }

        Button buttonCancelReminder = findViewById(R.id.buttonCancelReminder);
        Button buttonSaveReminder = findViewById(R.id.buttonSaveReminder);

        buttonCancelReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("fromReminderButton",fromReminderButton);
                setResult(RESULT_CANCELED,resultIntent);
                finish();
            }
        });

        buttonSaveReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int minute = timePicker.getMinute();
                int hour = timePicker.getHour();
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth() + 1;
                int year = datePicker.getYear();
                Intent resultIntent = new Intent();
                resultIntent.putExtra("fromReminderButton",fromReminderButton);
                resultIntent.putExtra("enableReminder",true);
                resultIntent.putExtra("minute",minute);
                resultIntent.putExtra("hour",hour);
                resultIntent.putExtra("day",day);
                resultIntent.putExtra("month",month);
                resultIntent.putExtra("year",year);
                setResult(RESULT_OK,resultIntent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        setResult(RESULT_CANCELED,resultIntent);
        finish();
    }
}