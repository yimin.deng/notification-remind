package com.example.nota;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class EditNoteActivity extends AppCompatActivity {
    private ConstraintLayout edit_note;
    private Button buttonSaveNote;
    private Button buttonCancel;
    private Button buttonDeleteNote;
    private Button buttonReminderTime;
    private Switch switchReminder;
    private EditText editTitle;
    private EditText editText;
    private NotesOperations notesOperations;
    private Note note;
    int id;
    String title;
    String text;
    int minute;
    int hour;
    int day;
    int month;
    int year;
    boolean reminderTimePresent;
    boolean firstEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        notesOperations = MainActivity.notesOperations;

        // Obtain the views of all widgets at startup
        edit_note = (ConstraintLayout) findViewById(R.id.edit_note);
        switchReminder = (Switch) findViewById(R.id.switchReminder);
        buttonSaveNote = (Button) findViewById(R.id.buttonSaveNote);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonDeleteNote = (Button) findViewById(R.id.buttonDeleteNote);
        buttonDeleteNote.setBackgroundColor(Color.RED);
        editTitle = (EditText) findViewById(R.id.editTitle);
        editTitle.setFilters(new InputFilter[] {new InputFilter.LengthFilter(20)});
        editText = (EditText) findViewById(R.id.editText);

        // See if it is the first time editing the note
        SharedPreferences sp = getSharedPreferences("edit_note",Context.MODE_PRIVATE);
        firstEdit = sp.getBoolean("firstEdit",true);
        if (firstEdit) {

            // Use intent to retrieve note from database with id
            Intent i = getIntent();
            id = i.getIntExtra("id",-1);
            notesOperations.open();
            note = notesOperations.retrieveNoteFromId(id);
            notesOperations.close();

            // Load variables from note
            text = note.getText();
            if (note.getTitle() != null) {
                title = note.getTitle();
            }
            if (note.getReminderTime() != null) {
                LocalDateTime currentZoneReminderTime = note.getReminderTime().atZone(ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
                minute = currentZoneReminderTime.getMinute();
                hour = currentZoneReminderTime.getHour();
                day = currentZoneReminderTime.getDayOfMonth();
                month = currentZoneReminderTime.getMonthValue();
                year = currentZoneReminderTime.getYear();
                reminderTimePresent = true;
            }

            // Change firstEdit to false after all variables are loaded and save all variables to sp
            firstEdit = false;
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean("firstEdit",firstEdit);
            editor.putInt("id",id);
            editor.putString("text",text);
            if (note.getTitle() != null) {
                editor.putString("title",title);
            }
            if (note.getReminderTime() != null) {
                editor.putBoolean("reminderTimePresent",reminderTimePresent);
                editor.putInt("minute",minute);
                editor.putInt("hour",hour);
                editor.putInt("day",day);
                editor.putInt("month",month);
                editor.putInt("year",year);
            }
            editor.commit();

        } else {

            // Load all variables from sp
            id = sp.getInt("id",0);
            text = sp.getString("text","");
            if (sp.contains("title")) {
                title = sp.getString("title","");
            }
            reminderTimePresent = sp.getBoolean("reminderTimePresent",false);
            if (reminderTimePresent) {
                minute = sp.getInt("minute",-1);
                hour = sp.getInt("hour",-1);
                day = sp.getInt("day",-1);
                month = sp.getInt("month",-1);
                year = sp.getInt("year",-1);
            }

        }

        // Display loaded variables for user
        editText.setText(text);
        editTitle.setText(title);
        if (reminderTimePresent) {
            activateReminderButton(switchReminder);
        }

        // Activate listener for switch
        switchReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent toInputTimeActivity = new Intent();
                    toInputTimeActivity.setClass(getApplicationContext(),InputReminderActivity.class);
                    toInputTimeActivity.putExtra("reminderTimePresent",reminderTimePresent);
                    if (reminderTimePresent) {
                        toInputTimeActivity.putExtra("minute",minute);
                        toInputTimeActivity.putExtra("hour",hour);
                        toInputTimeActivity.putExtra("day",day);
                        toInputTimeActivity.putExtra("month",month);
                        toInputTimeActivity.putExtra("year",year);
                    }
                    startActivityForResult(toInputTimeActivity,1);
                } else {
                    if (buttonReminderTime != null) {
                        destroyReminderButton();
                    }
                }
            }
        });

        buttonSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("edit_note",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.commit();

                Intent i = new Intent();
                i.setClass(getApplicationContext(),MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        buttonDeleteNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notesOperations.open();
                notesOperations.deleteNote(id);
                notesOperations.close();
                Toast.makeText(getApplicationContext(),R.string.note_deleted,Toast.LENGTH_SHORT).show();

                SharedPreferences sp = getSharedPreferences("edit_note",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.commit();

                Intent i = new Intent();
                i.setClass(getApplicationContext(),MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            boolean fromReminderButton = data.getBooleanExtra("fromReminderButton",false);
            if (resultCode == RESULT_CANCELED) {
                if (fromReminderButton) {} else {
                    switchReminder.setChecked(false);
                }
            } else {
                minute = data.getIntExtra("minute",0);
                hour = data.getIntExtra("hour",0);
                day = data.getIntExtra("day",0);
                month = data.getIntExtra("month",0);
                year = data.getIntExtra("year",0);
                SharedPreferences sp = getSharedPreferences("edit_note",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt("minute",minute);
                editor.putInt("hour",hour);
                editor.putInt("day",day);
                editor.putInt("month",month);
                editor.putInt("year",year);
                editor.commit();
                reminderTimePresent = true;
                activateReminderButton(switchReminder);
            }
        }
    }

    @Override
    public void onBackPressed() {
        String title = editTitle.getText().toString();
        String text = editText.getText().toString();
        Note note;
        if (text.equals("") == false) {
            SharedPreferences sp = getSharedPreferences("edit_note",Context.MODE_PRIVATE);
            year = sp.getInt("year",0);
            month = sp.getInt("month",0);
            day = sp.getInt("day",0);
            hour = sp.getInt("hour",0);
            minute = sp.getInt("minute",0);
            Switch switchReminder = (Switch) findViewById(R.id.switchReminder);
            if (title.length() == 0) {
                if (switchReminder.isChecked()) {
                    note = new Note(text, year, month, day, hour, minute);
                } else {
                    note = new Note(text);
                }
            } else {
                if (switchReminder.isChecked()) {
                    note = new Note(title, text, year, month, day, hour, minute);
                } else {
                    note = new Note(title, text);
                }
            }
            note.setId(id);
            notesOperations.open();
            notesOperations.modifyNote(note,1);
            notesOperations.close();
            Toast.makeText(getApplicationContext(),R.string.note_saved, Toast.LENGTH_SHORT).show();

            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            editor.commit();

            Intent i = new Intent();
            i.setClass(getApplicationContext(),MainActivity.class);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(getApplicationContext(),R.string.please_type_something,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        SharedPreferences sp = getSharedPreferences("edit_note",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
        super.onDestroy();
    }

    public void activateReminderButton(Switch switchReminder) {

        // Set switch to on
        switchReminder.setChecked(true);

        if (buttonReminderTime != null) {
            buttonReminderTime.setVisibility(View.VISIBLE);
            LocalDateTime timeToDisplay = LocalDateTime.of(year,month,day,hour,minute);
            buttonReminderTime.setText(timeToDisplay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        } else {
            // Add new button to indicate reminderTime and allow user to click on it to change it
            buttonReminderTime = new Button(getApplicationContext());
            buttonReminderTime.setId(R.id.buttonReminderTime);
            edit_note.addView(buttonReminderTime);
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(edit_note);
            constraintSet.connect(buttonReminderTime.getId(),ConstraintSet.RIGHT,R.id.edit_note,ConstraintSet.RIGHT,0);
            constraintSet.connect(buttonReminderTime.getId(),ConstraintSet.LEFT,R.id.edit_note,ConstraintSet.LEFT,0);
            constraintSet.connect(buttonReminderTime.getId(),ConstraintSet.TOP,R.id.switchReminder,ConstraintSet.BOTTOM,0);
            constraintSet.applyTo(edit_note);
            LocalDateTime timeToDisplay = LocalDateTime.of(year,month,day,hour,minute);
            buttonReminderTime.setText(timeToDisplay.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
        }

        buttonReminderTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toInputTimeActivity = new Intent();
                toInputTimeActivity.setClass(getApplicationContext(),InputReminderActivity.class);
                toInputTimeActivity.putExtra("reminderTimePresent",reminderTimePresent);
                if (reminderTimePresent) {
                    toInputTimeActivity.putExtra("minute",minute);
                    toInputTimeActivity.putExtra("hour",hour);
                    toInputTimeActivity.putExtra("day",day);
                    toInputTimeActivity.putExtra("month",month);
                    toInputTimeActivity.putExtra("year",year);
                    toInputTimeActivity.putExtra("fromReminderButton",true);
                }
                startActivityForResult(toInputTimeActivity,1);
            }
        });
    }

    public void destroyReminderButton() {
        buttonReminderTime.setVisibility(View.GONE);
    }
}