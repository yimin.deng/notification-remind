package com.example.nota;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Vector;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static NotesOperations notesOperations;
    private Button button_add_note;
    private ListView listViewDisplayDDL;
    private ListView listViewDisplayALL;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //show notes with upcoming reminders (earliest reminders first)  and all notes (last modified first).
        notesOperations = new NotesOperations(getApplicationContext());
        listViewDisplayDDL = findViewById(R.id.listViewDisplayDDL);
        listViewDisplayALL = findViewById(R.id.listViewDisplayAll);
        notesOperations.open();
        Vector<Note> lNotesDDL = notesOperations.listAllNotes(true);
        Vector<Note> lNotesALL = notesOperations.listAllNotes(false);
        notesOperations.close();
        ArrayAdapter<Note> arrayAdapterDDL = new ArrayAdapter<Note>(this,android.R.layout.simple_list_item_1, lNotesDDL);
        ArrayAdapter<Note> arrayAdapterALL = new ArrayAdapter<Note>(this,android.R.layout.simple_list_item_1, lNotesALL);

        // Initiate the links between listView and ArrayAdapter
        listViewDisplayDDL.setAdapter(arrayAdapterDDL);
        listViewDisplayALL.setAdapter(arrayAdapterALL);

        listViewDisplayDDL.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Recover the note for the selected item in the listView and put its Id into an intent
                Note note = (Note) parent.getAdapter().getItem(position);
                Intent i = new Intent();
                i.putExtra("id",note.getId());
                i.setClass(getApplicationContext(),EditNoteActivity.class);
                startActivity(i);
                finish();
            }
        });

        listViewDisplayALL.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Recover the note for the selected item in the listView and put its Id into an intent
                Note note = (Note) parent.getAdapter().getItem(position);
                Intent i = new Intent();
                i.putExtra("id",note.getId());
                i.setClass(getApplicationContext(),EditNoteActivity.class);
                startActivity(i);
                finish();
            }
        });

        button_add_note = findViewById(R.id.MainAct_Btn_Add_Note);
        button_add_note.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();
        if (v == button_add_note) {
            i.setClass(getApplicationContext(), AddNoteActivity.class);
            startActivity(i);
            finish();
        }
    }
}