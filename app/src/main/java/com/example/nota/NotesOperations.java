package com.example.nota;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Vector;

public class NotesOperations {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public NotesOperations(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    public void modifyNote(Note note,int mode) {
        // mode : 0 to add note, any other value (not 0) to update note
        ContentValues values = new ContentValues();
        values.put(dbHelper.getText(),note.getText());
        values.put(dbHelper.getLastModified(),note.getLastModified().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
        if (note.getTitle() != null) {
            values.put(dbHelper.getTitle(),note.getTitle());
        } else {
            values.putNull(dbHelper.getTitle());
        }
        if (note.getReminderTime() != null) {
            values.put(dbHelper.getReminderTime(),note.getReminderTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
        } else {
            values.putNull(dbHelper.getReminderTime());
        }
        if (mode == 0) {
            database.insert(dbHelper.getTableNotes(),null,values);
        } else {
            database.update(dbHelper.getTableNotes(),values,"id=" + note.getId(),null);
        }
    }

    public Vector<Note> listAllNotes(boolean onlyReminder) {
        Vector<Note> listNotes = new Vector<Note>();
        String[] columns = new String[5];

        columns[0] = dbHelper.getId();
        columns[1] = dbHelper.getTitle();
        columns[2] = dbHelper.getText();
        columns[3] = dbHelper.getLastModified();
        columns[4] = dbHelper.getReminderTime();

        Cursor cursor;
        if (onlyReminder) {
            cursor = database.rawQuery("SELECT * FROM " + dbHelper.getTableNotes() + " WHERE " + dbHelper.getReminderTime() + " IS NOT NULL AND " + dbHelper.getReminderTime() + " > datetime('now') ORDER BY " + dbHelper.getReminderTime() + " ASC;",null);
        } else {
            cursor = database.query(dbHelper.getTableNotes(),columns,null,null,null,null,dbHelper.getLastModified() + " DESC");
        }

        if (cursor.moveToFirst() == true) {
            do {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
                Note note;
                int id = cursor.getInt(0);
                String text = cursor.getString(2);
                String lastModifiedRaw = cursor.getString(3);
                LocalDateTime lastModified = LocalDateTime.parse(lastModifiedRaw,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
                if ((cursor.getType(1) == Cursor.FIELD_TYPE_STRING) && (cursor.getType(4) == Cursor.FIELD_TYPE_STRING)) {
                    String title = cursor.getString(1);
                    String reminderTimeRaw = cursor.getString(4);
                    LocalDateTime reminderTime = LocalDateTime.parse(reminderTimeRaw,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
                    note = new Note(id,title,text,lastModified,reminderTime);
                } else if (cursor.getType(4) == Cursor.FIELD_TYPE_STRING) {
                    String reminderTimeRaw = cursor.getString(4);
                    LocalDateTime reminderTime = LocalDateTime.parse(reminderTimeRaw,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
                    note = new Note(id,text,lastModified,reminderTime);
                } else if (cursor.getType(1) == Cursor.FIELD_TYPE_STRING) {
                    String title = cursor.getString(1);
                    note = new Note(id,title,text,lastModified);
                } else {
                    note = new Note(id,text,lastModified);
                }
                listNotes.add(note);
            } while (cursor.moveToNext() == true);
        }
        return listNotes;
    }

    public Note retrieveNoteFromId(int id) {
        Cursor cursor;
        cursor = database.rawQuery("SELECT * FROM " + dbHelper.getTableNotes() + " WHERE " + dbHelper.getId() + "=" + Integer.toString(id),null);
        cursor.moveToFirst();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        Note note;
        String text = cursor.getString(2);
        String lastModifiedRaw = cursor.getString(3);
        LocalDateTime lastModified = LocalDateTime.parse(lastModifiedRaw,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
        if ((cursor.getType(1) == Cursor.FIELD_TYPE_STRING) && (cursor.getType(4) == Cursor.FIELD_TYPE_STRING)) {
            String title = cursor.getString(1);
            String reminderTimeRaw = cursor.getString(4);
            LocalDateTime reminderTime = LocalDateTime.parse(reminderTimeRaw,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
            note = new Note(id,title,text,lastModified,reminderTime);
        } else if (cursor.getType(4) == Cursor.FIELD_TYPE_STRING) {
            String reminderTimeRaw = cursor.getString(4);
            LocalDateTime reminderTime = LocalDateTime.parse(reminderTimeRaw,DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
            note = new Note(id,text,lastModified,reminderTime);
        } else if (cursor.getType(1) == Cursor.FIELD_TYPE_STRING) {
            String title = cursor.getString(1);
            note = new Note(id,title,text,lastModified);
        } else {
            note = new Note(id,text,lastModified);
        }
        return note;
    }

    public void deleteNote(int id) {
        database.delete(dbHelper.getTableNotes(),"id=" + id,null);
    }

    public void close() {
        dbHelper.close();
    }

}
