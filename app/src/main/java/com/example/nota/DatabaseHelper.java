package com.example.nota;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database version
    private static int databaseVersion = 1;
    // Database file name
    private static final String databaseName = "notes.db";
    // Name of table to be created in the database
    private static String tableNotes = "notes";
    // Columns in the table
    private static final String id = "id";
    private static final String title = "title";
    private static final String text = "text";
    private static final String lastModified = "last_modified";
    private static final String reminderTime = "reminder_time";
    // SQL query used to create table "notes" in database
    private static final String tableCreationQuery = "CREATE TABLE " + tableNotes + "(" +
            id + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            title + " TEXT, " +
            text + " TEXT NOT NULL, " +
            lastModified + " TEXT NOT NULL, " +
            reminderTime + " TEXT);";
    // SQL query used to delete table "notes in database
    private static final String tableDeletionQuery = "DROP TABLE IF EXISTS " + tableNotes + ";";

    // Constructor for DatabaseHelper class
    public DatabaseHelper(Context context) {
        super(context, databaseName,null, databaseVersion);
    }

    // Create table "notes" in database
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tableCreationQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(tableDeletionQuery);
        onCreate(db);
    }

    public int getDatabaseVersion() {
        return databaseVersion;
    }

    public String getTableNotes() {
        return tableNotes;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public static String getLastModified() {
        return lastModified;
    }

    public static String getReminderTime() {
        return reminderTime;
    }

}
